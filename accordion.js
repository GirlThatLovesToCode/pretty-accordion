$(document).ready(function() {
    $( ".outer" ).click(function() {
        $(this).closest(".tab").toggleClass("active");
        $(this).siblings(".hide").slideToggle("slow");

        $(this).closest(".tab").siblings(".tab.active").find(".hide").slideUp("slow")
        $(this).closest(".tab").siblings(".tab.active").removeClass("active")
    })
})